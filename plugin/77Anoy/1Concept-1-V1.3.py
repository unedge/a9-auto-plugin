# 自定义寻路文件
LL = []
L = [20,21,24,25,48,49]
RR = [70,71,72]
R = [7,8,15,52,53,57,58,69]
B = ["16-1.5","17-0.5","40-2","41-1","43-1.5","55-1","65-2","66-1"]
BB = [27]
YY_1 = [35,36,72,73,80,81,84,85,90,91]#单喷
YY_2 = [1,2,13,14,18,19,28,29,50,51,59,60,63,64,67,68,94,95,97,98]#紫喷

# （固定代码勿动）
B = {int(item.split('-')[0]): float(item.split('-')[1]) for item in B}

for index in range(5000):

# 从比赛详情页进入至车辆详情
     time.sleep(3)
     pro.press_button('A', 0)
     time.sleep(3)
     page = OCR.get_page()
     logger.info(f"page name = {page.name}")
     if page.name != "select_car":
        pro.press_button('A', 0)
     time.sleep(3)
     #pro.press_button("DPAD_RIGHT", 0)
     time.sleep(1)
     pro.press_button('A', 0)

# 检测play按钮是否有效（固定代码勿动）
     time.sleep(3)
     page = OCR.get_page()
     logger.info(f"page name = {page.name}")
     mode = consts.car_hunt_zh
     states_a = OCR.has_play(mode)
     logger.info(f"states_a = {states_a}")
     mode = consts.legendary_hunt_zh
     states_b = OCR.has_play(mode)
     logger.info(f"states_b = {states_b}")
     mode = consts.custom_event_zh
     states_c = OCR.has_play(mode)
     logger.info(f"states_c = {states_c}")
     if (states_a or states_b or states_c or page.name == "searching" or page.name == "loading_race" or page.name == "racing") and page.name == "car_info":
        pro.press_button('A', 0)
        time.sleep(3)
        page = OCR.get_page()
        logger.info(f"page name = {page.name}")
        if page.name != "car_info":
           logger.info(f"racing")
           break
        else:
           logger.info(f"upup")

# 强制复位比赛详情页
           time.sleep(3)
           pro.press_group(['B'] * 10, 1)
           time.sleep(3)
           pro.press_button('A', 0)
           time.sleep(3)
           pro.press_group(['ZR'] * 6, 1)
           time.sleep(3)
           pro.press_button('A', 0)     

     else:

# 强制复位比赛详情页
        time.sleep(3)
        pro.press_group(['B'] * 10, 1)
        time.sleep(3)
        pro.press_button('A', 0)
        time.sleep(3)
        pro.press_group(['ZR'] * 6, 1)
        time.sleep(3)
        pro.press_button('A', 0)

# 检测页面蓝币重新进入（固定代码勿动，仅可以修改蓝币买票开关）
blue_coin = 0 # 蓝币开关（0-不使用蓝币买票，1-使用蓝币买票）
for ticket_check in range(1000):
     time.sleep(3)
     page = OCR.get_page()
     logger.info(f"page name = {page.name}")
     if page.name == "tickets":
        if blue_coin == 0:
           pro.press_group(['B', 'B', 'A', 'A'], 3)
        elif blue_coin == 1:
           pro.press_group(['A', 'DPAD_DOWN', 'A', 'B', 'A'], 2)
           break
     else:
        break

# 寻路文件按进度执行（固定代码勿动）
completed = []    
for index in range(1000):
      progress = OCR.get_progress()
      logger.info(f"progress = {progress}")
      if progress == -1:
         page = OCR.get_page()
         logger.info(f"page name = {page.name}")
      if progress == -1 and (page.name == "race_score" or page.name == "race_results"):
         break
      if progress in completed:
         continue

      # 特殊进度优先部分开始（以下可修改,若无需特殊进度，可删除）

      # 特殊进度优先部分结束（固定代码勿动）
      if progress in LL:
         pro.press_group(['DPAD_LEFT'] * 2, 0.1)
      if progress in L:
         pro.press_button("DPAD_LEFT", 0)
      if progress in RR:
         pro.press_group(['DPAD_RIGHT'] * 2, 0.1)
      if progress in R:
         pro.press_button("DPAD_RIGHT", 0)
      if progress in B:
         duration = B.get(progress)
         pro.press_buttons("B", down=duration)
      if progress in BB:
         pro.press('B')
         time.sleep(0.1)
         pro.release('B')
         time.sleep(0.1)
         pro.press('B')
         time.sleep(0.1)
         pro.release('B')
      if progress in YY_1:
         pro.press_button("Y", 0)
      if progress in YY_2:
         pro.press_group(['Y'] * 2, 0.1)
      completed.append(progress)

# 正常领包复位比赛详情页
time.sleep(3)
pro.press_button('B', 0)
time.sleep(3)
pro.press_button('B', 0)
time.sleep(8)
pro.press_button('B', 0)
