# 自定义寻路文件
LL = [3,23,25,36,37,38,72,73,81,82]
L = [0,18,19]
RR = [33,34,94,95]
R = [44]
B = ["4-1.5","12-4","32-1","51-1.5","57-1","66-1","90-0.1"]
BB = [75]
YY_1 = [92,93]#单喷
YY_2 = [1,2,5,6,7,16,17,24,27,28,29,35,42,43,47,48,49,50,54,55,58,59,62,63,68,69,74,76,77,78,86,87,88,96,97,98]#紫喷
REPEAT_1 = [0,3,23,75]#需要重复执行操作的进度，比如图比较长，同一个进度需要多次操作才能对应上选路时机

# （固定代码勿动）
B = {int(item.split('-')[0]): float(item.split('-')[1]) for item in B}

for index in range(5000):

# 从比赛详情页进入至车辆详情
     time.sleep(3)
     pro.press_button('A', 0)
     time.sleep(3)
     page = OCR.get_page()
     logger.info(f"page name = {page.name}")
     if page.name != "car_info":
        pro.press_button('A', 0)

# 检测play按钮是否有效（固定代码勿动）
     time.sleep(3)
     page = OCR.get_page()
     logger.info(f"page name = {page.name}")
     mode = consts.car_hunt_zh
     states_a = OCR.has_play(mode)
     logger.info(f"states_a = {states_a}")
     mode = consts.legendary_hunt_zh
     states_b = OCR.has_play(mode)
     logger.info(f"states_b = {states_b}")
     mode = consts.custom_event_zh
     states_c = OCR.has_play(mode)
     logger.info(f"states_c = {states_c}")
     if (states_a or states_b or states_c or page.name == "searching" or page.name == "loading_race" or page.name == "racing") and page.name == "car_info":
        pro.press_button('A', 0)
        time.sleep(3)
        page = OCR.get_page()
        logger.info(f"page name = {page.name}")
        if page.name != "car_info":
           logger.info(f"racing")
           break
        else:
           logger.info(f"upup")

# 强制复位比赛详情页
           time.sleep(3)
           pro.press_group(['B'] * 10, 1)
           time.sleep(3)
           pro.press_button('A', 0)
           #time.sleep(3)
           #pro.press_group(['ZR'] * 6, 1)
           time.sleep(3)
           pro.press_button('A', 0)     

     else:

# 强制复位比赛详情页
        time.sleep(3)
        pro.press_group(['B'] * 10, 1)
        time.sleep(3)
        pro.press_button('A', 0)
        #time.sleep(3)
        #pro.press_group(['ZR'] * 6, 1)
        time.sleep(3)
        pro.press_button('A', 0)

# 检测页面蓝币重新进入（固定代码勿动，仅可以修改蓝币买票开关）
blue_coin = 0 # 蓝币开关（0-不使用蓝币买票，1-使用蓝币买票）
for ticket_check in range(1000):
     time.sleep(3)
     page = OCR.get_page()
     logger.info(f"page name = {page.name}")
     if page.name == "tickets":
        if blue_coin == 0:
           pro.press_group(['B', 'B', 'A', 'A'], 3)
        elif blue_coin == 1:
           pro.press_group(['A', 'DPAD_DOWN', 'A', 'B', 'A'], 2)
           break
     else:
        break

# 寻路文件按进度执行（固定代码勿动）
completed = []
index_a = 0
index_b = 0
index_c = 0
for index in range(1000):
      progress = OCR.get_progress()
      logger.info(f"progress = {progress}")
      if progress == -1:
         index_c = index_b + 1
         index_b = index_a + 1
         index_a = index
         if index_a == index_b and index_b == index_c:
            page = OCR.get_page()
            logger.info(f"page name = {page.name}")
            if progress == -1 and index >= 100 and (page.name == "race_score" or page.name == "race_results"):
               break
      if progress in completed:
         continue

      # 特殊进度优先部分开始（以下可修改,若无需特殊进度，可删除）

      # 特殊进度优先部分结束（固定代码勿动）
      if progress in LL:
         pro.press_group(['DPAD_LEFT'] * 2, 0.1)
      if progress in L:
         pro.press_button("DPAD_LEFT", 0)
      if progress in RR:
         pro.press_group(['DPAD_RIGHT'] * 2, 0.1)
      if progress in R:
         pro.press_button("DPAD_RIGHT", 0)
      if progress in B:
         duration = B.get(progress)
         pro.press_buttons("B", down=duration)
      if progress in BB:
         pro.press('B')
         time.sleep(0.1)
         pro.release('B')
         time.sleep(0.1)
         pro.press('B')
         time.sleep(0.1)
         pro.release('B')
         time.sleep(0.1)
         pro.press('B')
         time.sleep(0.1)
         pro.release('B')
      if progress in YY_1:
         pro.press_button("Y", 0)
      if progress in YY_2:
         pro.press('Y')
         time.sleep(0.1)
         pro.release('Y')
         time.sleep(0.1)
         pro.press('Y')
         time.sleep(0.1)
         pro.release('Y')
         time.sleep(0.1)
         pro.press('Y')
         time.sleep(0.1)
         pro.release('Y')
      if progress not in REPEAT_1:
         completed.append(progress)

# 正常领包复位比赛详情页
time.sleep(3)
pro.press_button('B', 0)
time.sleep(3)
pro.press_button('B', 0)
time.sleep(8)
pro.press_button('B', 0)
